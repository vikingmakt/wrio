from envelop import Envelop
from event import Event
from output import Output
from rask.main import Main
from rask.parser.json import dictfy
from rask.parser.utcode import UTCode
from rask.options import define,options
from rask.rmq import Announce,RMQ
from response import Response
from rfour import RFour

__all__ = ['Run']

class Run(Main):
    __settings = {
        'exchange':{
            'headers':'wrio_headers',
            'topic':'wrio'
        },
        'header_prefix':'w-r-h-',
        'header_status':{
            'default':200,
            'key':'w-status',
            'response':[200,201,202,204],
            'no_body':[204,304]
        },
        'services':{}
    }

    @property
    def __http_routes__(self):
        try:
            assert self.__http_routes
        except (AssertionError,AttributeError):
            self.__http_routes = [
                (r'/.*',Event,{'response':self.response,'rmq':self.rmq,'utcode':self.utcode},'event')
            ]
        except:
            raise
        return self.__http_routes

    @property
    def rmq(self):
        try:
            assert self.__rmq
        except (AssertionError,AttributeError):
            self.__rmq = RMQ(options.wrio['rmq'])
        except:
            raise
        return self.__rmq

    @property
    def response(self):
        try:
            assert self.__response
        except (AssertionError,AttributeError):
            self.__response = Response()
        except:
            raise
        return self.__response

    def after(self):
        self.log.info('WRIO')
        return True

    def before(self):
        self.ioengine.loop.add_callback(self.settings_load)
        return True

    def services_init(self,*args):
        def on_channel(_):
            options.wrio['output'] = Output(channel=_,utcode=self.utcode).push
            return True

        self.rmq.channel('output',future=self.ioengine.future(on_channel))
        options.wrio['envelop'] = Envelop(self.utcode).pack
        options.wrio['rfour'] = RFour(self.rmq).request
        return True

    def settings_check(self,_=None):
        try:
            assert _.next() in options.wrio
        except AssertionError:
            self.log.error('No valid settings!')
            self.ioengine.stop()
            return False
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.settings_check,
                _=iter([
                    'rmq'
                ])
            )
        except StopIteration:
            def on_channel(_):
                Announce(
                    channel=_,
                    settings=options.wrio,
                    future=self.ioengine.future(self.services_init)
                )
                return True

            self.rmq.channel('announce',future=self.ioengine.future(on_channel))
            return True
        except:
            raise
        else:
            self.ioengine.loop.add_callback(self.settings_check,_)
        return None

    def settings_load(self):
        try:
            rfile = open('settings.json','r')
            rconf = dictfy(rfile.read())
            rfile.close()
            assert rconf
        except AssertionError:
            self.log.error('No valid json!')
            self.ioengine.stop()
            return False
        except IOError:
            self.log.error('No settings file found!')
        except:
            raise
        else:
            self.__settings.update(rconf)

        define('wrio',default=self.__settings)
        self.ioengine.loop.add_callback(self.settings_check)
        return True
