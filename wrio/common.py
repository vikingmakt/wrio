from rask.options import options

__all__ = [
    'header_assert',
    'header_cut',
    'header_export',
    'header_prefix_len',
    'header_status_assert',
    'header_status_export',
    'path_lstrip',
    'path_lrstrip',
    'path_rstrip',
    'path_to_rk',
    'path_segments'
]

def header_assert(_):
    try:
        assert _.startswith(options.wrio['header_prefix'])
    except (AssertionError,AttributeError):
        return False
    except:
        raise
    return True

def header_cut(k):
    return k[header_prefix_len():]

def header_export(k,b):
    try:
        assert k in b
        assert header_assert(k)
    except AssertionError:
        raise TypeError
    except:
        raise
    return (header_cut(k),b[k])

def header_prefix_len():
    return len(options.wrio['header_prefix'])

def header_status_assert(h):
    try:
        assert int(h[options.wrio['header_status']['key']]) in options.wrio['header_status']['response']
    except:
        raise
    return True

def header_status_export(h):
    try:
        header_status_assert(h)
    except AssertionError:
        return (int(h[options.wrio['header_status']['key']]),False)
    except KeyError:
        return (options.wrio['header_status']['default'],True)
    except:
        raise
    return (int(h[options.wrio['header_status']['key']]),True)

def path_lstrip(_):
    try:
        assert len(_) > 1
    except AssertionError:
        return _
    except:
        raise
    return _.lstrip('/')

def path_lrstrip(_):
    return path_rstrip(path_lstrip(_))

def path_rstrip(_):
    try:
        assert len(_) > 1
    except AssertionError:
        return _
    except:
        raise
    return _.rstrip('/')

def path_segments(_):
    _ = path_lrstrip(_).split('/')

    try:
        assert len(_) == 2
        assert _[0] == ''
        assert _[1] == ''
    except AssertionError:
        return (_,len(_))
    except:
        raise
    return ([],0)

def path_to_rk(_):
    try:
        assert len(_) > 1
    except AssertionError:
        return '/'
    except:
        raise
    return path_lrstrip(_).replace('/','.')    
