from rask.base import Base
from rask.options import options
from rask.rmq import BasicProperties

__all__ = ['Output']

class Output(Base):
    def __init__(self,channel,utcode):
        self.channel = channel.result().channel

        self.log.info('started')

    def push(self,body,headers,rk):
        self.channel.basic_publish(
            body=body,
            exchange=options.wrio['exchange']['topic'],
            properties=BasicProperties(headers=headers),
            routing_key=rk
        )
        return True
  
