from rask.http import asynchronous,Handler
from rask.options import options

__all__ = ['Event']

class Event(Handler):
    @property
    def envelop(self):
        try:
            assert self.__envelop
        except (AssertionError,AttributeError):
            self.__envelop = {
                'body':self.request.body,
                'headers':dict(self.request.headers),
                'method':self.request.method,
                'path':self.request.path,
                'query':self.request.query_arguments
            }
        except:
            raise
        return self.__envelop

    @property
    def rfour_err(self):
        try:
            assert self.__rfour_err
        except (AssertionError,AttributeError):
            self.__rfour_err = {
                'NOT_ROUTED':404
            }
        except:
            raise
        return self.__rfour_err

    def future(self,_):
        try:
            self.set_status(self.rfour_err[_.result()['__err__']])
            self.finish()
        except KeyError:
            self.ioengine.loop.add_callback(self.response_headers,response=_.result())
        except:
            raise
        return True

    def request_nowait(self):
        def on_envelop(_):
            options.wrio['output'](**_.result())
            return True

        options.wrio['envelop'](
            future=self.ioengine.future(on_envelop),
            **self.envelop
        )

    @asynchronous
    def request_wait(self):
        def on_envelop(_):
            options.wrio['rfour'](
                future=self.ioengine.future(self.future),
                exchange=options.wrio['exchange']['topic'],
                **_.result()
            )
            return True

        options.wrio['envelop'](
            future=self.ioengine.future(on_envelop),
            **self.envelop
        )
        return None

    def response_body(self,response):
        self.set_status(response['headers'].get('w-status',options.wrio['header_status']['default']))

        try:
            assert response['headers']['w-status'] in options.wrio['header_status']['no_body']
        except (AssertionError,KeyError):
            self.write(response['body'])
        except:
            raise

        self.finish()
        return True

    def response_headers(self,response,i=None):
        try:
            header = i.next()
            assert header.startswith('w-r-h-')
        except AssertionError:
            pass
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.response_headers,
                response=response,
                i=iter(response['headers'])
            )
            return True
        except StopIteration:
            self.ioengine.loop.add_callback(self.response_body,response=response)
            return True
        except:
            raise
        else:
            self.set_header(header[6:],response['headers'][header])

        self.ioengine.loop.add_callback(
            self.response_headers,
            response=response,
            i=i
        )
        return True

    def initialize(self,response,rmq,utcode):
        self.delete = self.request_nowait
        self.get = self.request_wait
        self.head = self.request_nowait
        self.patch = self.request_nowait
        self.post = self.request_wait
        self.put = self.request_nowait

        self.response = response
        self.rmq = rmq
        self.utcode = utcode
        return True
