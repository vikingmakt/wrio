from common import path_rstrip,path_lrstrip,path_segments,path_to_rk
from rask.base import Base

__all__ = ['Envelop']

class Envelop(Base):
    def __init__(self,utcode):
        self.utcode = utcode

    def __headers_encode__(self,_,future,i=None,r=None):
        try:
            k = i.next()
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.__headers_encode__,
                _=_,
                future=future,
                i=iter(_),
                r={}
            )
        except StopIteration:
            future.set_result(r)
            return True
        except:
            raise
        else:
            r['w-h-%s' % k] = _[k]

            self.ioengine.loop.add_callback(
                self.__headers_encode__,
                _=_,
                future=future,
                i=i,
                r=r
            )
        return None

    def headers_encode(self,result,headers,future):
        def on_encode(_):
            result['headers'].update(_.result())
            future.set_result(result)
            return True

        self.ioengine.loop.add_callback(
            self.__headers_encode__,
            _=headers,
            future=self.ioengine.future(on_encode)
        )
        return True

    def pack(self,body,headers,method,path,query,future):
        try:
            body = unicode(body)
        except UnicodeDecodeError:
            pass
        except:
            raise

        self.ioengine.loop.add_callback(
            self.split_segments,
            result={
                'body':body,
                'headers':{
                    'w-method':method,
                    'w-path':path_rstrip(path),
                    'w-query':query
                },
                'rk':path_to_rk(path)
            },
            headers=headers,
            future=future
        )
        return True
   
    def split_segments(self,result,headers,future,i=None,_=None):
        try:
            result['headers']['w-segment-%s' % i] = _.next()
        except AttributeError:
            s = path_segments(result['headers']['w-path'])
            result['headers']['w-segments-length'] = s[1]

            self.ioengine.loop.add_callback(
                self.split_segments,
                result=result,
                headers=headers,
                future=future,
                i=0,
                _=iter(s[0])
            )
        except StopIteration:
            self.ioengine.loop.add_callback(
                self.headers_encode,
                result=result,
                headers=headers,
                future=future
            )
        except:
            raise
        else:
            self.ioengine.loop.add_callback(
                self.split_segments,
                result=result,
                headers=headers,
                future=future,
                i=i + 1,
                _=_
            )
        return True
