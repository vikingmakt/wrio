from common import header_export,header_status_export
from rask.base import Base

__all__ = ['Response']

class Response(Base):
    def finish(self,io):
        io.finish()
        return True
    
    def header_setter(self,io,_):
        io.set_header(_[0],_[1])
        return True
    
    def headers(self,io,h,f,i=None):
        try:
            self.header_setter(
                io,
                header_export(i.next(),h)
            )
        except AttributeError:
            self.ioengine.loop.add_callback(
                self.headers,
                io=io,
                h=h,
                i=iter(h),
                f=f
            )
            return False
        except StopIteration:
            f.set_result(True)
            return True
        except TypeError:
            pass
        except:
            raise

        self.ioengine.loop.add_callback(
            self.headers,
            io=io,
            h=h,
            i=i,
            f=f
        )
        return None
    
    def main(self,io,h,b):
        def on_status(_):
            try:
                assert _.result()
            except AssertionError:
                self.ioengine.loop.add_callback(
                    self.finish,
                    io=io
                )
            except:
                raise
            else:
                def on_headers(r):
                    io.write(b)
                    
                    self.ioengine.loop.add_callback(
                        self.finish,
                        io=io
                    )
                    return True
        
                self.ioengine.loop.add_callback(
                    self.headers,
                    io=io,
                    h=h,
                    f=self.ioengine.future(on_headers)
                )
            return True
        
        self.ioengine.loop.add_callback(
            self.status_code,
            io=io,
            h=h,
            f=self.ioengine.future(on_status)
        )
        return True

    def status_code(self,io,h,f):
        try:
            r = header_status_export(h)
            io.set_status(r[0])
            assert r[1]
        except AssertionError:
            f.set_result(False)
        except:
            raise
        else:
            f.set_result(True)
        return True
