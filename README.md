# WRIO

## Changelog

|Version|Description|
|----|----|
|0.0.3|Response code detached from event, another flavor, returning 404 to no routed messages and more stuff|
|0.0.2|Split path segments|
|0.0.1|Start of something awesome, using rfour as "protocol".|

